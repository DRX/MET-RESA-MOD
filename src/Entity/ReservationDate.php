<?php

namespace Drupal\reservation\Entity;

use Drupal\reservation\ReservationDateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 *
 * @ingroup reservation
 *
 *
 * @ContentEntityType(
 *   id = "reservation_date",
 *   label = @Translation("reservationdate entity"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "reservation_date",
 *   entity_keys = {
 *     "id" = "rdid"
 *   }
 * )
 *
 */
class ReservationDate extends ContentEntityBase implements ReservationDateInterface {

    use EntityChangedTrait;

    /**
     * @param EntityStorageInterface $storage_controller
     * @param array $values
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
      parent::preCreate($storage_controller, $values);
      $values += [
        'user_id' => \Drupal::currentUser()->id(),
      ];
    }

    /**
     * @return mixed
     */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }

    /**
     * @return mixed
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }

    /**
     * @return mixed
     */
    public function getOwner() {
      return $this->get('user_id')->entity;
    }

    /**
     * @return mixed
     */
    public function getReservationRessourceNode() {
      return $this->get('nid')->entity;
    }

    /**
     * @return mixed
     */
    public function getOwnerId() {
      return $this->get('user_id')->target_id;
    }

    /**
     * @return mixed
     */
    public function getDate() {
      return $this->get('date')->value;
    }

    /**
     * @param $format
     * @return string
     */
    public function getDateFormat($format) {
        $date = new \DateTime($this->getDate());
        return $date->format($format);      
    }

    /**
     * @return mixed
     */
    public function getStatut() {
      return $this->get('statut')->value;
    }

    /**
     * @param $statut
     * @return $this
     */
    public function setStatut($statut) {
      $this->set('statut', $statut ? '1' : '0');
      return $this;
    }

    /**
     * @return mixed
     */
    public function getHoraire() {
      return $this->get('horaire')->value;
    }

    /**
     * @return mixed
     */
    public function getJaugeStatut() {
      return $this->get('jauge_statut')->value;
    }

    /**
     * @return mixed
     */
    public function getJaugeNombre() {
      return $this->get('jauge_nombre')->value;
    }

    /**
     * @return mixed
     */
    public function getRappel() {
      return $this->get('rappel')->value;
    }

    /**
     * @return mixed
     */
    public function getRappelJour() {
      return $this->get('rappel_jour')->value;
    }

    /**
     * @return mixed
     */
    public function getEnquete() {
      return $this->get('enquete')->value;
    }

    /**
     * @return mixed
     */
    public function getEnqueteJour() {
      return $this->get('enquete_jour')->value;
    }

    /**
     * @return mixed
     */
    public function getPublie() {
      return $this->get('publie')->value;
    }

    /**
     * @param $publie
     * @return $this
     */
    public function setPublie($publie) {
      $this->set('publie', $publie ? '1' : '0');
      return $this;
    }

    /**
     * @param $horaire
     * @return $this
     */
    public function setHoraire($horaire) {
      $this->set('horaire', $horaire ? '1' : '0');
      return $this;
    }

    /**
     * @param $uid
     * @return $this
     */
    public function setOwnerId($uid) {
      $this->set('user_id', $uid);
      return $this;
    }

    /**
     * @param UserInterface $account
     * @return $this
     */
    public function setOwner(UserInterface $account) {
      $this->set('user_id', $account->id());
      return $this;
    }

    /**
     * @return mixed
     */
    public function getDay() {
      return $this->get('day')->value;
    }

    /**
     * @param EntityTypeInterface $entity_type
     * @return mixed
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

      $fields['rdid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('ID'))
        ->setDescription(t('The ID of the reservationdate entity.'))
        ->setReadOnly(TRUE);

      $fields['uuid'] = BaseFieldDefinition::create('uuid')
        ->setLabel(t('UUID'))
        ->setDescription(t('The UUID of the reservationdate entity.'))
        ->setReadOnly(TRUE);

      $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('User Name'))
        ->setDescription(t('The Name of the associated user.'))
        ->setSetting('target_type', 'user')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'author',
          'weight' => 1,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 1,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['nid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Node ID'))
        ->setDescription(t('Node lié à la réservation.'))
        ->setSetting('target_type', 'reservation_ressource_node')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'title',
          'weight' => 2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 2,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['date'] = BaseFieldDefinition::create('datetime')
        ->setLabel(t('Day'))
        ->setDescription(t('Day to reservation'))
        ->setSettings([
          'max_length' => 2,
          'text_processing' => 0,
        ])
        ->setDefaultValue('')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 10,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 10,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('statut'))
        ->setDescription(t('The statut of the reservationressource entity.'))
        ->setDefaultValue(True)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 5,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 5,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['publie'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Publié'))
        ->setDefaultValue(False)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 30,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['horaire'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Horaire'))
        ->setDefaultValue(False)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 30,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['rappel'] = BaseFieldDefinition::create('boolean')
          ->setLabel(t('Rappel'))
          ->setDefaultValue(True)
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 40,
          ])
          ->setDisplayOptions('form', [
            'type' => 'boolean_checkbox',
            'weight' => 40,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['rappel_jour'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Rappel à J - '))
        ->setDefaultValue(3)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 41,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 41,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['enquete'] = BaseFieldDefinition::create('boolean')
          ->setLabel(t('Enquete'))
          ->setDefaultValue(True)
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 50,
          ])
          ->setDisplayOptions('form', [
            'type' => 'boolean_checkbox',
            'weight' => 50,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['enquete_jour'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Enquête de satisfaction à J + '))
        ->setDefaultValue(3)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 51,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 51,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['jauge_statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Statut Jauge'))
        ->setDefaultValue(1)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 60,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 60,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['jauge_nombre'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Jauge'))
        ->setDefaultValue(1)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 60,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 60,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);


      $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Created'))
        ->setDescription(t('The time that the entity was created.'));

      $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the entity was last edited.'));

      return $fields;
    }  

}
