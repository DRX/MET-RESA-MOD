<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationHoraire entity.
 *
 * @ingroup reservation
 */
interface ReservationHoraireInterface extends ContentEntityInterface, EntityChangedInterface {

}
