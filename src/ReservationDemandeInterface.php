<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationDate entity.
 *
 * @ingroup reservation
 */
interface ReservationDemandeInterface extends ContentEntityInterface, EntityChangedInterface {
    
    
}
