<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationRessource entity.
 *
 * @ingroup reservation
 */
interface ReservationRessourceNodeInterface extends ContentEntityInterface, EntityChangedInterface {
    
    
}
