<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationDate entity.
 *
 * @ingroup reservation
 */
interface ReservationDateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {
    
    
    
}
