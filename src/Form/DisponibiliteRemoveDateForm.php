<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteRemoveDateForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class DisponibiliteRemoveDateForm
 * @package Drupal\reservation\Form
 */
class DisponibiliteRemoveDateForm extends ConfirmFormBase {
    
    /**
     * The ID of the item to delete.
     *
     * @var string
     */
    protected $nid;

    /**
     * @return string
     */
    public function getFormId() {
      return 'disponibilite_remove_date_form';
    }

    /**
     * @return string
     */
    public function getQuestion() {
       //the question to display to the user.
       return 'Voulez vous annuler les dispos de la ressource ' . $this->nid . ' ?' ;
    }

    /**
     * @return Url
     */
    public function getCancelUrl() {
       //this needs to be a valid route otherwise the cancel link won't appear
       return new Url('reservation.disponibilite.index', ['choix' => 'user']);
    }

    /**
     * @return string
     */
    public function getDescription() {
       //a brief desccription
       return 'Êtes-vous sûr ?';
    }

    /**
     * @return string
     */
    public function getConfirmText() {
       return 'Suppression !';
    }


    /**
     * @return string
     */
    public function getCancelText() {
       return 'Retour au menu';
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @param null $nid
     * @return mixed
     */
    public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {
       $this->nid = $nid;
       return parent::buildForm($form, $form_state);
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $reservationDate = \Drupal::service('reservation.date');
        $reservationDate->setCancelDate($this->nid);

        $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
    }
}